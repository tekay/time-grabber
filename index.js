const fs = require('fs');
const fetch = require('node-fetch');
const { exit } = require('process');
const util = require('util')
let cliArgs = require('minimist')(process.argv.slice(2));
let debug = false;
let filter = true;
let startDate;
let endDate;
let mode = 'project';

let httpParams;
let config = {};

const HELP_TEXT = 'This program collects time tracking information from GitLab.\n\
\n\
--help shows this help text\n\
--debug enable debug output\n\
--start <date in format YYYY-MM-DD\n\
--end <date in format YYYY-MM-DD\n\
--no-filter does not filter out issues with 0h spent\n\
--mode project|issues\n\
\n\
Helper arguments to set up the config:\n\
--username <GitLab username> returns the userId\n\
--project <project path> returns project id'

try {
	if (fs.existsSync('./config.json')) {
		config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));
	}
} catch (e) {
	console.log(e);
	exit(1);
}

function isValidDate(dateString) {
  var regEx = /^\d{4}-\d{2}-\d{2}$/;
  if(!dateString.match(regEx)) return false;  // Invalid format
  var d = new Date(dateString);
  var dNum = d.getTime();
  if(!dNum && dNum !== 0) return false; // NaN value, Invalid date
  return d.toISOString().slice(0,10) === dateString;
}

function getNextLinkFromHeaders(headers) {
	const links = headers.get('Link').split(', ');
	for (const l of links) {
		const linkTuple = l.split('; ');
		if (linkTuple[1] === 'rel="next"') {
			return linkTuple[0].slice(1, -1);
		}
	}

	return null;
}

async function parseEnvVars() {
	if (process.env.HELP) {
		console.log(HELP_TEXT);
		exit(0);
	}

	if (process.env.DEBUG) {
		debug = true;
	}

	if (process.env.NO_FILTER) {
		filter = !process.env.NO_FILTER;
	}

	if (process.env.START) {
		cliArgs.start = process.env.START;
	}

	if (process.env.END) {
		cliArgs.end = process.env.END;
	}

	if (process.env.USERNAME) {
		cliArgs.username = process.env.USERNAME;
	}

	if (process.env.PROJECT) {
		cliArgs.project = process.env.PROJECT;
	}

	if (process.env.ACCESS_TOKEN) {
		config.accessToken = process.env.ACCESS_TOKEN;
	}

	if (process.env.GITLAB_URL) {
		config.gitlabUrl = process.env.GITLAB_URL;
	}

	if (process.env.USER_ID) {
		config.userId = +process.env.USER_ID;
	}

	if (process.env.PROJECT_IDS) {
		config.projectIds = process.env.PROJECT_IDS.split(',');
	}

	if (process.env.MODE) {
		cliArgs.mode = process.env.MODE;
	}
}

async function parseCliArgs() {
	if ((cliArgs.d) || (cliArgs.debug)) {
		debug = true;
	}

	if (cliArgs.filter !== undefined) {
		filter = false;
	}

	if (cliArgs.start) {
		if (!isValidDate(cliArgs.start)) {
			console.log('Couldn\'t parse start input as valid date');
			exit(1);
		}
		startDate = new Date(cliArgs.start);
	}

	if (cliArgs.end) {
		if (!isValidDate(cliArgs.end)) {
			console.log('Couldn\'t parse end input as valid date');
			exit(1);
		}
		endDate = new Date(cliArgs.end);
	}

	if (cliArgs.mode) {
		if (cliArgs.mode === 'project' || cliArgs.mode === 'issues') {
			mode = cliArgs.mode;
		}
	}
}

async function setup() {
	httpParams = {
		headers: {
			"PRIVATE-TOKEN": config.accessToken
		}
	};
}

async function triggerCliActions() {
	if ((cliArgs.h) || (cliArgs.help)) {
		console.log(HELP_TEXT);
		exit(0);
	}
	
	if (cliArgs.username) {
		if (cliArgs.username === '') {
			console.log('Need to give a username, exiting.');
			exit(1);
		}
		const username = cliArgs.username;
		const user = await getUser(username);
		const userId = user.id;
		console.log(`Username: ${username} - Id: ${userId}`);
		exit(0);
	}

	if (cliArgs.project) {
		if (cliArgs.project === '') {
			console.log('Need to give a project path, exiting.');
			exit(1);
		}
		const projectPath = cliArgs.project;
		const project = await getProject(projectPath);
		const projectId = project.id;
		console.log(`Project: ${projectPath} - Id: ${projectId}`);
		exit(0);
	}
}

async function getUser(username) {
	const usernameUrl = `${config.gitlabUrl}/api/v4/users?username=${username}`;
	const response = await fetch(usernameUrl, httpParams);
	const json = await response.json();
	return json[0];
}

async function getProject(projectPath) {
	const projectUrl = `${config.gitlabUrl}/api/v4/projects/${encodeURIComponent(projectPath)}`;
	const response = await fetch(projectUrl, httpParams);
	const json = await response.json();
	return json;
}

async function getProjectFromId(projectId) {
	const url = `${config.gitlabUrl}/api/v4/projects/${projectId}`;
	const response = await fetch(url, httpParams);
	const json = await response.json();
	return json;
}

async function getAllIssuesFromProjectId(projectId) {
	let url = `${config.gitlabUrl}/api/v4/projects/${projectId}/issues`;
	let issues = [];
	while (url !== null) {
		const response = await fetch(url, httpParams);
		const json = await response.json();
		issues = issues.concat(json);
		url = getNextLinkFromHeaders(response.headers);
	}

	return issues;
}

async function getAllDiscussionsFromIssue(issue) {
	let discussionUrl = `${config.gitlabUrl}/api/v4/projects/${issue.project_id}/issues/${issue.iid}/discussions`;
	let discussions = [];
	while (discussionUrl !== null) {
		const response = await fetch(discussionUrl, httpParams);
		const json = await response.json();
		discussions = discussions.concat(json);
		discussionUrl = getNextLinkFromHeaders(response.headers);
	}

	return discussions;
}

function getReferencedProjectPathFromIssue(issue) {
	// ToDo: Use regex or some other parsing. please....
	const refById = 'Contributes to #';
	const refByLink = `Contributes to ${config.gitlabUrl}`;

	const ret = {};

	if (!issue.description || issue.description == "") {
		return null;
	}

	if ((issue.description.indexOf(refById) > -1) && (issue.description.indexOf(';', issue.description.indexOf(refById)))) {
		const startOfIssueId = issue.description.indexOf(refById) + refById.length;
		const endOfIssueId = issue.description.indexOf(';', issue.description.indexOf(refById));

		ret.projectPath = issue.references.full.substring(0, issue.references.full.length - issue.references.short.length);
		ret.iissueId = Number(issue.description.substring(startOfIssueId, endOfIssueId));

		return ret;
	} else if ((issue.description.indexOf(refByLink) > -1) && (issue.description.indexOf(';', issue.description.indexOf(refByLink)))) {
		const issuePartString = '/-/issues/'

		// The +1 on the second param is to actually get the ';' for further parsing
		const referenceString = issue.description.substring(
			issue.description.indexOf(refByLink),
			issue.description.indexOf(';', issue.description.indexOf(refByLink)) + 1
		);

		// The +1 is because there is a '/' that needs to be taken care of
		const startOfProjectPath = referenceString.indexOf(config.gitlabUrl) + config.gitlabUrl.length + 1;
		const endOfProjectPath = referenceString.indexOf(issuePartString);

		const startOfIssueId = referenceString.indexOf(issuePartString) + issuePartString.length;
		const endOfIssueId = referenceString.indexOf(';');

		ret.projectPath = referenceString.substring(startOfProjectPath, endOfProjectPath);
		ret.iissueId = Number(referenceString.substring(startOfIssueId, endOfIssueId));

		return ret;
	}

	return null;
}

function getTimeInMinutesFromDiscussions(discussions) {
	const ADDED_SUBSTR = 'added ';
	const TIME_SPENT_SUBSTR = ' of time spent at ';
	const notes = discussions.map(d => d.notes).flat();
	const userNotes = notes.filter(d => d.author.id === config.userId);
	let userTimeNotes = userNotes.filter(d => d.body.indexOf(TIME_SPENT_SUBSTR) > -1);

	if (startDate) {
		userTimeNotes = userTimeNotes.filter(d => startDate <= new Date(d.created_at));
	}
	if (endDate) {
		userTimeNotes = userTimeNotes.filter(d => new Date(d.created_at) <= endDate);
	}

	const timeInMinutesArray = userTimeNotes.map(note => {
		const cutBefore = note.body.indexOf(ADDED_SUBSTR) + ADDED_SUBSTR.length;
		const cutAfter = note.body.indexOf(TIME_SPENT_SUBSTR);
		let timeString = note.body.substring(cutBefore, cutAfter);
		let minutes = 0;

		const hourIndex = timeString.indexOf('h');
		if (hourIndex > -1) {
			const hourValue = parseInt(timeString.substr(0, hourIndex));
			minutes = minutes + hourValue * 60;
			if (timeString.indexOf(' ') > -1) {
				timeString = timeString.substr(hourIndex + 2);
			}
		}

		const minuteIndex = timeString.indexOf('m');
		if (minuteIndex > -1) {
			const minuteValue = parseInt(timeString.substr(0, minuteIndex));
			minutes = minutes + minuteValue;
			timeString = timeString.substr(minuteIndex);
		}

		return minutes;
	});

	const timeInMinutes = timeInMinutesArray.reduce(((acc, cur) => acc + cur), 0);

	return timeInMinutes;
}

function aggregateOverReferences(issues) {
	for (const i of issues) {
		let target = i.referenced;
		let targetIssue;
		while (target) {
			const targetIssueArray = issues.filter(ii => (ii.projectPath === target.projectPath) && (ii.iissueId === target.iissueId));
			if (targetIssueArray) {
				targetIssue = targetIssueArray[0];
				target = targetIssue.referenced;
			} else {
				console.log(`Could not find the target. Path: ${target.projectPath}, IIssueId: ${target.iissueId}`);
			}
		}
		if (targetIssue) {
			targetIssue.timeInMinutes = targetIssue.timeInMinutes + i.timeInMinutes;
			i.timeInMinutes = 0;
		}
	}
}

function printProjectOutput(data) {
	data.map(d => {
		if (d.totalTimeInMinutes === 0 && filter) {
			return;
		}
		console.log('##################################################');
		console.log(`Project: ${d.name}`);
		console.log(`Total time spent on project: ${d.totalTimeInMinutes / 60}h`);
		d.issues.map(i => {
			if (i.timeInMinutes === 0 && filter) {
				return;
			}
			console.log('========================================');
			console.log(`Issue: ${i.name}`);
			console.log(`Total time spent: ${i.timeInMinutes / 60}h`);
		});
	});
}

function printIssueOutput(issues) {
	issues.map(i => {
		if (i.timeInMinutes === 0 && filter) {
			return;
		}
		console.log('##################################################');
		console.log(`Project: ${i.projectPath}`);
		console.log(`Issue: ${i.name}`);
		console.log(`Total time spent: ${i.timeInMinutes / 60}h`);
	});
}

async function main() {
	await parseEnvVars();
	await parseCliArgs();
	await setup();
	await triggerCliActions();

	const data = await Promise.all(config.projectIds.map(async id => {
		const project = await getProjectFromId(id);
		const projectIssues = await getAllIssuesFromProjectId(id);

		const issueData = await Promise.all(projectIssues.map(async issue => {
			const discussionsData = await getAllDiscussionsFromIssue(issue);
			const timeInMinutes = getTimeInMinutesFromDiscussions(discussionsData);
			const referenced = getReferencedProjectPathFromIssue(issue);

			const issueRet = {
				name: issue.title,
				referenced: referenced,
				projectPath: project.path_with_namespace,
				iissueId: issue.iid,
				timeInMinutes: timeInMinutes
			};

			return issueRet;
		}));

		const totalProjectTimeInMinutes = issueData.reduce(((acc, cur) => acc + cur.timeInMinutes), 0);

		const ret = {
			name: project.name,
			totalTimeInMinutes: totalProjectTimeInMinutes,
			issues: issueData
		};

		return ret;
	}));

	if (debug) {
		console.log('DEBUG OUTPUT START');
		console.log(util.inspect(data, {showHidden: false, depth: null}));
		console.log('DEBUG OUTPUT END');
	}

	if (mode === 'issues') {
		const allIssues = data.map(project => {
			return project.issues;
		}).flat();

		aggregateOverReferences(allIssues);

		printIssueOutput(allIssues);
	} else {
		printProjectOutput(data);
	}
}

	//

main();
