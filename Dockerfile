ARG IMAGE_BASETAG=lts-buster
FROM node:$IMAGE_BASETAG

ENV NODE_ENV production

# Create app directory
WORKDIR /usr/src/app

RUN yarn --frozen-lockfile

# Bundle app source
COPY . .

CMD [ "node", "index.js" ]
