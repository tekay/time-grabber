# Time Grabber

This project gathers time information from GitLab.

It is at this point still a very low level project, needing a bit of setup and printing the results onto the console.
There are plans to create a web application around it, but for now it will stay this way.

## Setup

* Generate an GitLab API access token with the scope `read_api`
* Get the userId for your account on GitLab (e.g. by using this tool with the `USERNAME` filled with your login)
* Get the projectIds for all your projects you want to monitor (e.g. by using this tool with the `PROJECT` filled with the full name of your project - an example: `tekay/time-grabber`)

## How it works

It grabs all issues for each project you feed it and collects the time spent from the GitLab discussions which are made by the user you feed it.
It then aggregates all spent time for each issue and also aggregates it for the project the issues belong to.

## Modi

There are two different modi:
* `issues`: Shows the time spent per issue. This also supports the `Contributes to` mechanic
* `project`: Shows the time spent per project. You will still see the times spent on issues, but it will be structured under the project. Does currently not support the `Contributes to` mechanic.

Default mode is `project`.

### Contributes to

In order to aggregate times spent over multiple issues, you can annotate them.
If you for example have an issue that functions as a user story with many subissues, you may want to see the spent time aggregated over all issues that belong to the user story issue.

To do so, simply have a line in the issue description like follows:
`Contributes to #12;`
In this case, the `12` is an issue number in the same project.
This function also works overarching projects. Syntax would be as follows:
`Contributes to https://gitlab.com/tekay/time-grabber/-/issues/1;`
That way, GitLab will show a neat link to your issue, and this grabber can parse accordingly.

---
**IMPORTANT**

The semicolon is very important for this functionality to work.

The referenced issue has to be in a project the grabber reads from (e.g. has to be given the projectId the referenced issue is under).

---

## Usage with Docker

I recommend using this tool via the Docker image.
It is provided via the GitLab Docker registry.
`registry.gitlab.com/tekay/time-grabber:master`

Neccessary variables are passed into the container via env vars.
To run the container, a command looks like following:
`docker run -it -e GITLAB_URL=https://gitlab.com -e ACCESS_TOKEN=xxxxxxxxxxxxxx -e USER_ID=1 -e PROJECT_IDS=1,2 registry.gitlab.com/tekay/time-grabber:master`

If you're using this tool frequently, I recommend creating an `.env`-file to store all the variables in.

### VPN

If the GitLab instance is behind a VPN, just run the docker container with `--network host`.
You also need to set the DNS entry (see [this](https://robinwinslow.uk/fix-docker-networking-dns) - the part about the `/etc/docker/daemon.json`).

### Settings
| Environment variable               | Description                                                               |
| ---------------------------------- | ------------------------------------------------------------------------- |
| HELP                               | If set to true prints out the help                                        |
| DEBUG                              | If set to true turns on debug output                                      |
| NO_FILTER                          | If set to true shows issues and projects with 0h time spent               |
| START                              | Startdate to filter for a timeframe - format: YYYY-MM-DD                  |
| END                                | Enddate to filter for a timeframe   - format: YYYY-MM-DD                  |
| USERNAME                           | If set returns the userId for that username                               |
| PROJECT                            | If set returns the projectId for that project (e.g. `tekay/time-grabber`) |
| ACCESS_TOKEN                       | GitLab API access token                                                   |
| GITLAB_URL                         | Url of the GitLab instance (e.g. `https://gitlab.com`)                    |
| USER_ID                            | userId                                                                    |
| PROJECT_IDS                        | Comma-separated list of projectIds                                        |
| MODE                               | `issues` or `project`                                                     |

## Local usage without Docker

* Set up the config
* Install the dependencies: `yarn`
* Run it with `node index.js`.

### Config

Copy the `example.config.json` to `config.json` and fill out accordingly.
